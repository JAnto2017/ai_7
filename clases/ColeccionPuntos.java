package AI_7.clases;

import java.util.ArrayList;
import java.util.Random;

public class ColeccionPuntos {
    protected PuntoND[] ptsAprendizaje;
    protected PuntoND[] ptsGeneralizacion;

    //métodos
    PuntoND[] getPtsAprendizaje() {
        return ptsAprendizaje;
    }

    PuntoND[] getPtsGeneralizacion() {
        return ptsGeneralizacion;
    }

    //constructor ----------------------------------------
    /**
     * Recibe la cadena que se corresponde con la integralidad del archivo en forma de tabla, el número de salidas y el ratio que se corresponde con los puntos de aprendizaje.
     * Los puntos se leen y se crea uno por uno a partir de su contenido.
     * El conjunto de aprendizaje se crea tomando el nº de ejemplos necesarios, se elige de manera aleatorio entre los puntos restantes.
     * El conjunto de generalización se crea a partir de los ejemplos todavía no seleccionados.
     * 
     * La lectura del archivo se hará en el programa principal.
     */
    
    ColeccionPuntos(String[] _contenido,int _numSalidas,double _ratioAprendizaje) {
        //lectura del archivo total
        int numLineas = _contenido.length;
        ArrayList<PuntoND> puntos = new ArrayList<>();

        for (int i = 0; i < numLineas; i++) {
            puntos.add(new PuntoND(_contenido[i], _numSalidas));
        }

        //creación de los puntos de aprendizaje
        int numPtsAprendizaje = (int) (numLineas * _ratioAprendizaje);
        ptsAprendizaje = new PuntoND[numPtsAprendizaje];
        Random generador = new Random();

        for (int i = 0; i < numPtsAprendizaje; i++) {
            int indices = generador.nextInt(puntos.size());
            ptsAprendizaje[i] = puntos.get(indices);
            puntos.remove(indices);
        }

        //creación de los puntos de generalización
        ptsGeneralizacion = (PuntoND[]) puntos.toArray(new PuntoND[puntos.size()]);
    }
}
