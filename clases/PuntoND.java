package AI_7.clases;

/**
 * PuntoND
 * 
 * ejemplo que contiene N dimensiones.
 * Contiene una tabla de valores considerados como entradas y una tabla de valores de salida.
 * 
 * Hacemos los atributos públicos, se declaran como final, para que sean modificables.
 */
public class PuntoND {
    public final double[] entradas;
    public final double[] salidas;
    
    //constructor-----------------------------------------
    /**
     * Recibe como argumentos la cadena que se corresponde con la línea del archivo de texto y el número de salidas de los ejemplos.
     * 
     * El contenido se separa de los caracteres que se corresponden con la tecla tabulación \t gracias a la función split.
     */

    public PuntoND(String str, int _numSalidas) {
        String[] contenido = str.split("\t");
        entradas = new double[contenido.length - _numSalidas];

        for (int i = 0; i < entradas.length; i++) {
            entradas[i] = Double.parseDouble(contenido[i]);
        }

        salidas = new double[_numSalidas];

        for (int i = 0; i < _numSalidas; i++) {
            salidas[i] = Double.parseDouble(contenido[entradas.length + 1]);
        }
    }

}