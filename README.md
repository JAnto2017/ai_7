# REDES NEURONALES

Sistemas que pueden resolver algunos problemas complejos, sobre los que los sistemas clásicos están limitados, así nacieron las **redes neuronales aritficiales**.
___

## Machine Learning

> **ML** Son todas las técnicas que permiten a un algoritmo aprender a partir de ejemplos, sin programación directa de la resolución.
> La mayoría de las técnicas de **ML** son algoritmos puramente matemáticos y/o técnicas de inteligencia artificial.

___

## Formas de Aprendizaje

> El **ML** permite resolver diferentes tipos de problemas, que se reparten entre dos formas de aprendizaje principales.

### Aprendizaje No Supervisado

> No es la más habitual. Dónde en el aprendizaje no hay resultado esperado.
> Se utiliza la forma de aprendizaje para hacer el *clustering* (segmentación), con un conjunto de datos, buscamos determinar las clases de hechos. De esta manera, se maximiza la semejanza entre los datos de una misma clase, cuando los putos están próximos y minimizando cuando están separados.

### Aprendizaje Supervisado

> Es la forma de aprendizaje más habitual. Proporcionando un conjunto de ejemplos al algoritmo de aprendizaje, lo que permite; compara la salida obtenida por la red, con la salida esperada.

> Hay dos tareas principales en aprendizaje supervisado: la **regresión** y la **clasificación**.

### Aprendizaje por Refuerzo

> Se indica al algoritmo si la decisión tomada era correcta o no, después de algún tiempo de utilización. Se proporciona una información global. Sin embargo, el algoritmo no sabe exactamente qué debería haber decidido.

> El *aprendizaje por refuerzo* también se puede hacer gracias a los **metaheurísticos**. Permitiendo optimozar las funciones sin conocimientos a priori. Otra técnica generalmente usada es el **algoritmo genético**, gracias a la evolución, permiten optimizar los pesos y encontrar estrategias ganadoras, sin información particular sobre lo que se esperaba.

___

## Regresión y Algoritmo de Regresión Lineal

En la **regresión lineal** se busca trazar una recta, pasando junto a un conjunto de puntos.

Si llamamos *X* a los valores de entrada, que contienen *n* características, e *Y* a los datos de salida, buscamos encontrar las variables *t* que contiene *n+1* reales, que corresponden con los pesos o coeficientes de cada una de las características a las que se añade *t0* que es el sesgo o coordenada en el origen.

La dificultad reside en encontrar los valores correctos para los pesos. Como no existe solución sencilla ni matemática, si el número de ejemplos y de características es grande, se utiliza normalmente un *algoritmo iterativo* el **descenso por gradiente**.

Hay que recordar que se parte de una solución aleatoria.
Una vez que es posible determinar la calidad de una solución, hay un algoritmo que permite encontrar la mejor solución posible.

___

## Clasificación y Algoritmo de Separación

En este caso, en lugar de querer encontrar una recta que pase por todos los puntos, se pretende encontrar una que separe los puntos en dos categorías.

La ecuación general de la recta es la misma que para la regresión lineal, y se utiliza también el descenso por gradiente como algoritmo para mejorar una solución inicial aleatoria.

___

## Neurona formal y percepción

### Principios

* **Pesos** asignados a cada una de las entradas.
* Una **función de agregación**, que permite calcular un único valor a partir de las entradas y de los pesos correspondientes.
* Un **umbral** o sesgo, que permite indicar cuándo debe actuar la neurona.
* Una **función de activación**, que asocia a cada valor agregado un único valor de salida dependiendo del umbral.

### Red de tipo "perceptrón"

Un **perceptrón** es una red que contiene *p* neuronas formales. Cada una se relaciona con las *n* entradas. Esta red permite tener *p* salidas.

Cada neurona realiza la agregación y la activación, con pesos y umbral diferente al de otras neuronas. 

Las dos más habituales son:
+ Suma ponderada.
+ Cálculo de distancias.

En el caso de **suma ponderada**, simplemente se va a hacer la suma de todas las entradas (*E*), multiplicadas por sus pesos (*w*). Es lo que hace la regresión lineal o logística.

En el **cálculo de las distancias**, se va a comparar las entradas con los pesos y calcular la distancia entre las dos.

### Función de activación

Una vez que se ha calculado un valor único, la neurona compara este valor con un umbral y decide la salida. Para ello se pueden utilizar varias funciones.

[X] La función signo o **heavyside**, devuelve *+1* ó *0*. Si el valor agregado calculado es más grande que el umbral, devuelve **+1**, si no **0** ó **-1** según las aplicaciones.

[X] La función **sigmoidea** utiliza una exponencial. También se llama *función logística*. Matemáticamente se define por: *f(x) = 1 / 1 + e^-x*. Esta función permite un aprendizaje más fácil, gracias a su pendiente, sobre todo si los valores agregados están cercanos a 0. La derivada utilizada durante el aprendizaje es: *f'(x) = f(x) · (1 - f(x)).

[X] La función **gausiana**, es simétrica, con un máximo obtenido en *0*. Siendo *'k* y *k* las constantes dependientes de la desviación estándar deseada *f(x) = k · e^(⁻x²/k')*. La función es derivable, lo que permite un aprendizaje. Solo tiene un efecto local alrededor del umbral, y no sobre el espacio de búsqueda completo. La expresión es *f(x) = max(x,0)*. Su derivada vale 1 si f(x) es estrictamente positivo y 0 en caso contrario.

[X] La función **Softmax**, se utiliza en clasificación cuando no es suficiente con tomar la clase que obtiene el valor más fuerte, sino que se persigue obtener las probabilidades de pertenencia a una clase. Esta función permite agregar diferentes salidas y normalizar los valores, de manera que la suma sea 1. Cada valor de salida se corresponde directamente con el porcentaje de pertenencia. No necesita de aprendizaje.

### Ejemplo de red

Entradas binarias.
La función de agregación es la suma ponderada y la función de activación es una función *heavyside*.

Para una entrada *(1,0,1)*. Calculamos la suma ponderada entre las entradas y los pesos: *Suma = 1·0,5 + 1·0,5 + 1·0,5 = 1*.

A continuación se compara la suma con el umbral. Aquí la suma es superior al umbral, por lo tanto la salida vale *1*.

Para una entrada *(0,1,0)*, la suma valdría *0,5*, siendo inferior al umbral, por lo tanto en este caso la salida vale *0*.

La tabla muestra los valores de entradas posibles y salidas obtenidas.

| X1 | X2 | x3 | Salida |
| --- | --- | --- | --- |
| 0 | 0 | 0 | 0 |
| 0 | 0 | 1 | 0 |
| 0 | 1 | 0 | 0 |
| 0 | 1 | 1 | 1 |
| 1 | 0 | 0 | 0 |
| 1 | 0 | 1 | 1 |
| 1 | 1 | 0 | 1 |
| 1 | 1 | 1 | 1 |

Vemos que la neurona devuelve 1 cuando hay al menos dos entradas verdaderas y 0 en caso contrario. Esta función es difícil de expresar en forma de Y u O, pero para una red de neuronas es sencillo.
___

### Aprendizaje

Si seleccionar los pesos y el umbral manualmente no es posible, se utiliza un algoritmo de aprendizaje. Como para la regresión lineal y la regresión logística, se utilizará el **método de descenso por gradiente**.
___

## Red feed-fordward

Estas redes permiten superar los límites de los perceptores.

### Red con capa oculta

Compuesta por una o varias capas ocultas de neuronas, relacionadas con las entradas o las capas anteriores y una capa de salida, relacionada con las neuronas ocultas.

Se llama **feed-forward** porque la información solo puede ir desde las entradas hasta las salidas, sin volver atrás.

Cuantas más capas tiene la red y más complejas son, permite un mejor aprendizaje y más rápido, que con una o dos capas ocultas.

Las redes que utilizan neuronas de tipo perceptrón, se llaman **MLP** por *Multi Layer Perceptron*, mientras que las que utilizan neuronas con función de activación gaussiana, se llaman **RBF** por *Radial Basis Function*.

### Aprendizaje por retropropagación del gradiente

En el caso de las redes feed-forward, existe un aprendizaje posible, por **retropropagación del gradiente** llamado **Backpropagation** en inglés. Es una extensión del algoritmo de descenso por gradiente.

### Sobre-aprendizaje

La red aprende a partir de los datos que se le proporcionan y va a encontrar una función global, que permite limitar los errores.

Al inicio, el error será importante y después disminuirá para cada camino de datos de ejemplo y ajuste de los pesos.

La red va a aprender los puntos proporcionados y perder completamente en generalización, sobre todo si los datos proporcionados son ligeramente erróneos o poco numerosos; entonces tenemos **sobre-aprendizaje**.

Para evitar el sobre-aprendizaje o al menos detectarlo, vamos a separar nuestro conjunto de datos en tres sub-conjuntos:

1. conjunto de aprendizaje, contiene el 60% de los ejemplo.
2. conjunto de generalización, contiene el 20% de los ejemplos.
3. conjunto de validación, determina la calidad de la red.
   
## Mejoras del algoritmo

Estas mejoras permiten facilitar la convergencia acelerando el aprendizaje y limitando la varianza.

1. Batch, mini-batch y gradiente estocástico.
2. Regulación.
3. Dropout.
4. Variación del algoritmo de descenso por gradiente.
5. Creación de nuevos datos: data augmentation.
___

# Otras arquitecturas

Las redes *feed-forward* se utilizan mucho, pero no son las únicas redes.

## Red de neuronas con consolación

Las **redes neuronales consolativas**, están adaptadas para trabajar sobre las imágenes. Están compuestas de múltiples capas con funciones diferentes. Estas redes son muy difíciles de entrenar partiendo de 0.

## Mapas de Kohonen

Contienen una matriz de neuronas. A lo largo del tiempo, cada neurona se va a asociar a una zona del espacio de entrada, desplazándose por la superficie de ésta. 

## Red de neuronas recurrentes

La información tratada en una etapa se puede utilizar para el tratamiento de las siguientes entradas. En la actualidad se utilizan masivamente en todas las aplicaciones que tratan datos secuenciales, como el reconocimiento y el tratamiento de la palabra.

## Red de Hopfield

Son redes completamiento conectadas, donde cada neurona está relacionada con el resto. El aprendizaje en estas redes se hace gracias a una variante de la **ley de Hebb**.

## Dominios de aplicación

Son una técnica muy correcta cuando los criterios siguientes se cumplen:
- Hay muchos ejemplos disponibles para el aprendizaje.
- No existe relaciones conocidas entre las entradas y las salidas expresadas por las funciones.
- La salida es más importante que la manera de obtenerla, las redes neuronales no permiten tener una explicación sobre el proceso utilizado internamente.

## Reconocimiento de patterns

En esta tarea, diferentes *patterns* se presentan a la red durante el aprendizaje. Cuando se deben clasificar nuevos ejemplos, entones se pueden reconocer los motivos: se trata de una tarea de clasificación.

## Estimación de funciones

Consiste en asignar un valor numérico a partir de entradas, generalizando la relación existente entre ellas. Las entradas pueden representar características o series temporales, según las necesidades.

## Creación de comportamientos

Se trata de un comportamiento que podrá ser definido por ésta. Las aplicaciones en robótica y en la industria son numerosas.

## Aplicaciones actuales

Hay disponibles *frameworks* y también *plataformas* en los clouds. El **Deep Learning** y las redes neuronales también se utilizan mucho en los sistemas de reconocimiento facial.
___

# Implementación

Los **MLP** (*Multiplayer Perceptron*) son redes feedforward con neuronas de tipo perceptrón. La función de agregación es una suma ponderada y la función de activación una sigmoidea.

A continuación se presentan dos problemas:
1. XOR, permite probar si los algoritmos funcionan.
2. *Abalone*, se utiliza para comparar algoritmos de aprendizaje.

## Puntos y conjuntos de puntos

Necesitan muchos puntos para el aprendizaje, no es conveniente introducirlos a mano en el código. Por lo tanto, se utilizan archivos de texto con tabulaciones.